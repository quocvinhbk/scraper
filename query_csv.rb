require 'csv'
require_relative './helpers'
# require 'faker'
headers = ['id', 'name', 'email', 'city', 'street', 'country']

print_memory_usage do
  print_time_spent do
    # CSV.open('fk_data.csv', 'r', headers: headers) do |csv|
    CSV.foreach('fk_data.csv', headers: true) do |row|
      # 1_000_000.times do |i|
      #   name    = Faker::Name.name
      #   email   = Faker::Internet.unique.email
      #   city    = Faker::Address.city
      #   street  = Faker::Address.street_name
      #   country = Faker::Address.country
      #   csv << [i, name, email, city, street, country]
      # end
      puts row[0] + '--' + row[1] + '--' + row[2] + '--' + row[3] + '--' + row[4] if row['email'] == 'sherman@padberg.io'
      puts row[0] + '--' + row[1] + '--' + row[2] + '--' + row[3] + '--' + row[4] if row['email'] == 'koby.howell@spencer.info'
    end
  end
end