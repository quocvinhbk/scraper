require 'csv'
require_relative './helpers'
require 'faker'
headers = ['id', 'name', 'email', 'city', 'street', 'country']

# name    = "Pink Panther"
# email   = "pink.panther@example.com"
# city    = "Pink City"
# street  = "Pink Road"
# country = "Pink Country"

# log time
# SSD
#   Row num:   1000         #       10_000        #      100_000         #    1_000_000
#   Time:      0.67 s       #       3.24 s        #      32.0 s          #    352.78 s
#   Memory:    14.75 MB     #       16.49 MB      #      37.22 MB        #    251.91 MB

print_memory_usage do
  print_time_spent do
    CSV.open('data.csv', 'w', write_headers: true, headers: headers) do |csv|
      1_000.times do |i|
        name    = Faker::Name.name
        email   = Faker::Internet.unique.email
        city    = Faker::Address.city
        street  = Faker::Address.street_name
        country = Faker::Address.country
        csv << [i, name, email, city, street, country]
      end
    end
  end
end

