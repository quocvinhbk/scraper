# @connection = ActiveRecord::Base.connection
#   result = @connection.exec_query('SELECT * FROM events.potluck')
#   result.each do |row|
#     puts row
#   end

# client = Mysql2::Client.new(:host => "localhost", :username => "root")

# create_table = client.query(" CREATE TABLE events.Users ( 
#                                     id INT NOT NULL AUTO_INCREMENT,
#                                     PRIMARY KEY(id),
#                                     name    VARCHAR(55),
#                                     email   VARCHAR(255),
#                                     INDEX email_id (email),
#                                     street  VARCHAR(255),
#                                     country VARCHAR(255),
#                                     date TIMESTAMP        ) " )

require 'mysql2'
@client = Mysql2::Client.new(:host => "localhost", :username => "root")
@client.query_options.merge!( :symbolize_keys => true)

@email = 'foo@bar.com'
@results = @client.query("SELECT id FROM  events.Users WHERE email = '#{@email}' " )

if @results.count == 0
    str_query = "INSERT INTO events.Users (name,email,street,country) 
                                    VALUES ('foo','#{@email}',
                                            '12 Avanue','NYK') "
    @client.query(str_query)
    @results = @client.query("SELECT id FROM  events.Users WHERE email = '#{@email}' " )
# else
#   x = results.map { |result| result[:id] } 
#   x[0]
end

  x = @results.map { |result| result[:id] } 
  puts x[0]

def update_sql(table, values, condition)
  sql_values = values.map { |col,value| "#{col} = '#{value }' " }
  sql_where  = condition.map { |col,value| "#{col} = '#{value}' " }
  sql = "UPDATE #{table} SET #{sql_values.join(', ') } WHERE #{sql_where.join(' AND ') } "
  @client.query(sql)
end

user_data = { #id:       row['id'],
              name:     "Vietnamese", #"foo''s",
              email:    'foo@bar.com',
              street:   'NYC',
              country:  'Earth',
              # date: Time.zone.now
}

update_sql('events.Users', user_data, {email: 'foo@bar.com'})

# str_query = "UPDATE events.Users SET name = 'foo''sbar' WHERE email = 'foo@bar.com' " 
# @client.query(str_query)
