require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'csv'
require_relative './helpers'


def wrcsv
  headers = ['id', # [0]
             'title', # [1]
             'company_name', # [2]
             'city', # [3]
             'year_experience', # [4]
             'industries', # [5]
             'position', # [6]
             'salary', # [7]
             'update_date', # [8]
             'exp_time', # [9]
             'description', # [10]
             'job_requirements', # [11]
             'job_others', # [12]
             'job_id_careerbuilder', # [13]
             'job_link', # [14]
             'company_address', # [15]
             'company_email', # [16]
             'company_phone', # [17]
             'company_number_of_employees', # [18]
             'company_description', # [19]
             'company_link', # [20]
             'company_type', # [21]
             'company_id'] # [22]

  CSV.open('jobs.csv', 'wb', write_headers: true, headers: headers) do |jobsfile|
    CSV.foreach('links.csv', headers: true) do |links_row|
      data_arr = []
      # url = links_row['link']
      # url = 'https://careerbuilder.vn/vi/tim-viec-lam/ky-thuat-vien.35AF0862.html'
      sample = 'https://careerbuilder.vn/vi/tim-viec-lam/ky-thuat-vien.35AF0862.html'
      # sample = 'https://careerbuilder.vn/vi/tim-viec-lam/cua-hang-truong-–-tp-hcm-store-manager.35AF1374.html'
      url = normalize_uri(sample)
      page = Nokogiri::HTML(open(url))
      page.css('div.LeftJobCB').each do |object|
        # [0] id
        data_arr[0] = links_row['id']
        # [1] title
        # data_arr[1] = object.xpath('//*[@id="uni_container"]/div[4]/div[1]/div[1]/div[1]/div[2]/h1').text
        data_arr[1] = object.at_css('div.top-job > div.top-job-info > h1').inner_text
        # [2] company_name
        data_arr[2] = object.xpath('//*[@id="uni_container"]/div[4]/div[1]/div[1]/div[9]/p/span').text
        # [3] city
        data_arr[3] = object.xpath('//*[@id="showScroll"]/ul/li[1]/p[1]/b/a').text
        # [4] year_experience
        data_arr[4] = object.xpath('//*[@id="showScroll"]/ul/li[2]/p[1]/text()').text.strip
        # [5] industries
        data_arr[5] = object.xpath('//*[@id="showScroll"]/ul/li[3]/p[1]/b').text.gsub(',', '+')
        # data_arr[5] = object.css('div.box2Detail > ul.DetailJobNew > li.bgLine2 > p.fl_left > b').text
        # [6] position
        data_arr[6] = object.xpath('//*[@id="showScroll"]/ul/li[1]/p[2]/label').text
        # [7] salary
        data_arr[7] = object.xpath('//*[@id="showScroll"]/ul/li[2]/p[2]/label').text
        # [8] update_date
        data_arr[8] = object.xpath('//*[@id="uni_container"]/div[4]/div[1]/div[1]/div[2]/span').text
        # [9] exp_time
        data_arr[9] = object.xpath('//*[@id="showScroll"]/ul/li[3]/p[2]/text()').text.strip
        # [10] description
        data_arr[10] = object.xpath('//*[@id="uni_container"]/div[4]/div[1]/div[1]/div[6]/div').text
        # [11] job_requirements
        data_arr[11] = object.xpath('//*[@id="uni_container"]/div[4]/div[1]/div[1]/div[7]/div').text
        # [12] job_others
        data_arr[12] = object.xpath('//*[@id="uni_container"]/div[4]/div[1]/div[1]/div[8]/div').text
        # [13] job_id_careerbuilder
        data_arr[13] = ''
        # [14] job_link
        data_arr[14] = url
        # [15] company_address
        data_arr[15] = object.xpath('//*[@id="uni_container"]/div[4]/div[1]/div[1]/div[9]/p/label[1]/label/text()').text
        # [16] company_email
        data_arr[16] = ''
        # [17] company_phone
        data_arr[17] = ''
        # [18] company_number_of_employees
        data_arr[18] = object.xpath('//*[@id="uni_container"]/div[4]/div[1]/div[1]/text()').text.strip
        # [19] company_description
        data_arr[19] = object.xpath('//*[@id="emp_more"]/p[1]/text()').text
        # [20] company_link
        data_arr[20] = object.xpath('//*[@id="uni_container"]/div[4]/div[1]/div[1]/span').text
        # [21] company_type
        data_arr[21] = ''
        # [22] company_id
        data_arr[22] = ''

        puts data_arr[1]
        # (1..5).each do |i| 
        # puts data_arr[0]
        # puts data_arr[5]# unless data_arr[i].empty?
        # end
      end
      jobsfile << data_arr
    end
  end

#   CSV.foreach('jobs.csv', headers: true) do |row|
#     puts row['id']
#     puts row['title']
  # end
end

def normalize_uri(uri)
  return uri if uri.is_a? URI

  uri = uri.to_s
  uri, *tail = uri.rpartition "#" if uri["#"]

  URI(URI.encode(uri) << Array(tail).join)
end

# log time
#         no csv      #csv                #ventura
# Time:   203s         230.9s             152.87s
# Memory: 65 MB        70.41 MB           38.59 MB

print_memory_usage do
  print_time_spent do
    wrcsv
  end
end
