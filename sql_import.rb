require_relative './helpers'
require 'csv'
require 'mysql2'


def import 
  @client = Mysql2::Client.new(:host => "localhost", :username => "root")
  @client.query_options.merge!( :symbolize_keys => true)

  CSV.foreach('fk_data2.csv', headers: true) do |row|
    # headers = ['id', 'name', 'email', 'city', 'street', 'country']

    user_id = get_users_id(row)
    # user_data = { #id:       row['id'],
    #               name:     row['name'],
    #               email:    row['email'],
    #               street:   row['street'],
    #               country:  row['country'],
    #               # date: Time.zone.now.to_s(:db)
    #             }
    # update_sql('events.Users',user_data, id: user_id)
  end
end

def update_sql(table, values, condition)
  sql_values = values.map { |col,value| "#{col} = '#{value }' " }
  sql_where  = condition.map { |col,value| "#{col} = '#{value}' " }
  sql = "UPDATE #{table} SET #{sql_values.join(', ') } WHERE #{sql_where.join(' AND ') } "
  @client.query(sql)
end

## if email availble return id
## if email not create, create user and return id
def get_users_id(row)
  @results = @client.query("SELECT id FROM  events.Users WHERE email = '#{row['email']}'")
  if @results.count == 0
    # user not create yet, => create user
    str_query = "INSERT INTO events.Users (name,email,street,country) 
                            VALUES ('#{row['name'].gsub("'", "''")}'   ,'#{row['email'].gsub("'", "''")}',
                                    '#{row['street'].gsub("'", "''")}' ,'#{row['country'].gsub("'", "''")}') "
    @client.query(str_query)
    @results = @client.query("SELECT id FROM  events.Users WHERE email = '#{row['email']}'")
  end

  x = @results.map { |result| result[:id] } 
  return x[0]
end



print_memory_usage do
  print_time_spent do
    import
  end
end
