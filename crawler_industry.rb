require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'csv'
require_relative './helpers'
require 'yaml'
require 'redis'
require 'pry'

$r = Redis.new
def crawler_industry

  page_url = 'https://careerbuilder.vn/viec-lam/tat-ca-viec-lam-vi.html'
  page = Nokogiri::HTML(open(page_url))
  inds = page.xpath('//*[@id="industry"]')

  inds[0].children.each do |ind|
    code = ind['value'].scan(/\s?\-?\d+/).first
    slug = (code  == "-1")? "tat-ca-nganh-nghe" : ind['value'].scan(/[[a-z]|\-]+/).first
    name = ind.children.first.content
    # puts "#{code} -- #{slug} -- #{name}"
    $r.sadd('industry', YAML.dump([code,name,slug]))
  end

# # Retrive industry
  # inds_redis = Redis.new
  # inds_yaml_arr = inds_redis.smembers('industry')
  # inds_yaml_arr.each do |row|
  #   ind  = YAML.safe_load(row)
  #   #ind[0]: code
  #   #ind[1]: name
  #   #ind[2]: slug
  # end
end

print_memory_usage do
  print_time_spent do
    crawler_industry
  end
end