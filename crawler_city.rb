require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'csv'
require_relative './helpers'
require 'yaml'
require 'redis'
require 'pry'

require 'json'
require 'mysql2'




@client = Mysql2::Client.new(:host => "localhost", :username => "root")
$r = Redis.new

# # parse json file content: tinh_tp.json
# # to hash of code of city and city hash in a hash

# def parse_cities_from_json_to_redis
#   file = File.read('/home/vinhnq/Documents/train/scraper/tinh_tp.json')
#   file_hash = JSON.parse(file)
#   file_json = file_hash.to_json
#   $r_cities.sadd('cities', file_json)
# end

def find_city
  # rjobs = Redis.new
  # rjobs_arr = rjobs.smembers 'crawled'

  # rjobs_arr.each_with_index do |jobrow, id|
  #   break if id == 30
  #   job = YAML.load(jobrow)
  #   next if job[1].nil? || job[1].empty?
  #   get_city_id(job[3])
  # end
  # city_str = "Truyền hình / Báo chí / Biên tập+ Quảng cáo / Đối ngoại / Truyền Thông+ Tổ chức sự kiện"
  city_str = "Bình Dương,Hồ Chí Minh,Hồ Chí Minh,Bình Dương,Đồng Nai"
  city_array = city_str.split(',').map { |x| x.strip }
  # binding.pry
  city_array.each do |city|
    # get_industry_id(city)
    get_city_id(city)
  end
end

# Return id of city id database
def get_city_id(city_name = 'Hồ Chí Minh')
  city_query = @client.query("SELECT * FROM venjobs_app_development.cities WHERE name = '#{city_name}'")
  if city_query.any?
    city_query.each do |city|
      # return city['id']
      puts city['slug']
    end
  else
    # return 59
    puts "#--#"
  end
end

# Return id of industry id database
def get_industry_id(name = '--')
  inds = @client.query("SELECT * FROM venjobs_app_development.industries WHERE name = '#{name}'")
  if inds.any?
    inds.each do |ind|
      puts ind['slug']
    end
  else
    # return 59
    puts "#--#"
  end
end


# find_city



def crawler_city

  page_url = 'https://careerbuilder.vn/viec-lam/tat-ca-viec-lam-vi.html'
  page = Nokogiri::HTML(open(page_url))
  cities = page.xpath('//*[@id="location"]')

  cities[0].children.each_with_index do |city,id|
    # break if id == 3
    # binding.pry
    code = city['value'].scan(/\s?\-?\d+/).first
    slug = (code  == "-1")? "tat-ca-dia-diem" : city['value'].scan(/[[a-z]|\-]+/).first
    name = city.children.first.content.strip
    puts "#{code} -- #{slug} -- #{name}"
    # check if present
    # $r.sadd('cities', YAML.dump([code,name,slug]))
  end

  # # # Retrive city
  # cities_redis = Redis.new
  # cities_yaml_arr = cities_redis.smembers('cities')
  # cities_yaml_arr.each_with_index do |row,id|
  #   city  = YAML.safe_load(row)
  #   #city[0]: code
  #   #city[1]: name
  #   #city[2]: slug
  #   puts "#{id} -- #{city[0]} -- #{city[1]} -- #{city[2]}"
  # end

end

crawler_city
