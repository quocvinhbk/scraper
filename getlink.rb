
# #### TEST SOMETHING
# require 'rubygems'
# require 'nokogiri'
# require 'open-uri'
# require 'csv'


#   # posted_at = row.css('time').first.attributes["datetime"].value
#   # neighb_elem = row.css('span.result-hood')
#   # neighborhood = neighb_elem.any? ?  '@' + neighb_elem.inner_text.scan(/[^()]/).join : ''

#   # link = row.at_css('a.hdrlnk').attributes["href"].value


#   # puts "#{title} #{neighborhood}"
#   puts "Link: #{link}"
#   # puts "Posted at: #{posted_at} "
#   puts '########################################'
# end




#### TEST SOMETHING

# 1. get page 1 
#     - number of jobs
# 2. get all jobs link
#     - put link to csv file 
# 3. iterate all row of csv 
#     - follow link 
#     - get infomation 
#     - append to csv at same id of href 

# ### Sceranio 2: 
# 1. get page 1 
#   - number of jobs 
# 2. get job infor until not button with name trang ke
#   - get link array 
#   - each link of array 
#       - go to link 
#       - get information 
#       - append to csv with link too


require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'csv'
require_relative './helpers'

headers = ['id', 'name', 'email', 'city', 'street', 'country']


def initialize
  @job_sum = 0
  @job_link_num = 0
  @PAGE_URL = "https://careerbuilder.vn/viec-lam/tat-ca-viec-lam-vi.html"
  # @PAGE_URL = "https://careerbuilder.vn/viec-lam/tat-ca-viec-lam-trang-533-vi.html"
end

def getlink
    # 1. go to page 
    # 2. get link 
    # 3. co trang ke -> go to trang ke 
    # if not -> stop 

    loop do 
      page = Nokogiri::HTML(open(@PAGE_URL))
      page.css('div.gird_standard > dl > dd.brief' ).each do |row|
        @job_sum += 1
        title = row.css('span:nth-child(2) > h3.job > a').inner_text
        link = row.at_css('span:nth-child(2) > h3.job > a').attributes["href"].value
        puts "ID: #{@job_sum} && Link: #{link}"
        puts '#--#--#'
      end

      @btn_res = page.css('div.paginationTwoStatus > a.right')
      if @btn_res.any?
        @PAGE_URL = @btn_res[0].attributes["href"].value
      else 
        break
      end
    end
end


print_memory_usage do
  print_time_spent do
    initialize
    getlink
  end
end
